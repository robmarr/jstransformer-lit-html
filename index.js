const curry = require('lodash.curry')
const addLitHook = require('lit-hook')

exports.name = 'lit-html'
exports.inputFormats = ['lit', 'lit-html']
exports.outputFormat = 'html'

const render = curry((file, options, locals) => {
  const {exts, clearCache = true } = options || {}
  const revoke = addLitHook({file, exts, clearCache})
  const markup = require(file)(locals)
  revoke()
  return markup
})

exports.compileFile = (file, options) => locals => render(file, options, locals)

exports.compileFileAsync = (file, options) => {
  return new Promise((resolve, reject) => {
    let template
    try {
      template = render(file, options)
      resolve(locals => template(locals || {}))
    } catch (error) {
      reject(error)
    }
  })
}

