# jstransformer-lit-html

[![Coverage Status](https://coveralls.io/repos/bitbucket/robmarr/jstransformer-lit-html/badge.svg?branch=master)](https://coveralls.io/bitbucket/robmarr/jstransformer-lit-html?branch=master) [![](https://img.shields.io/bitbucket/pipelines/robmarr/jstransformer-lit-html.svg)](https://bitbucket.org/robmarr/jstransformer-lit-html/addon/pipelines/home) [![](https://img.shields.io/npm/v/jstransformer-lit-html/latest.svg)]()

A require hook that compiles lit-html files

## Installation

`npm i -S jstransformer-lit-html`

## API

Allows compilation of **lit-html** files into templates via a require hook.

```js
var lit = require('jstransformer')(require('jstransformer-lit-html'))

template = lit.compileFile('./template.lit')
//=> '<h1>${locals.title}</h1>
template({ title: 'Hello World!'})
//=> '<h1>Hello World!</h1>'
```

## License

Unless stated otherwise all works are:

- Copyright &copy; Robin Marr

and licensed under:

- [MIT License](http://spdx.org/licenses/MIT.html)

## Thanks!

Thanks to [Alexander Pope](https://github.com/popeindustries) for [lit-html-server](https://github.com/popeindustries/lit-html-server) it made this a breeze, also thanks to [Justin Fagnani](https://github.com/justinfagnani) and the [team](https://github.com/Polymer/lit-html/graphs/contributors) behind the [**lit-html**](https://github.com/Polymer/lit-html/) project!

